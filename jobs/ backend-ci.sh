#!/bin/bash

docker run --rm -v ${HOME}:/app -v $(pwd):/git alpine/git clone https://github.com/sergsoares/simple-tdd-with-11-steps.git
docker run --rm -i -v $(pwd)/simple-tdd-with-11-steps:/app composer/composer install

docker run --rm -i -v $(pwd)/trade-app-one-backend:/app composer/composer install

# docker run --rm -i -v $PWD:/root node /bin/bash -xe <<EOF
#   cd /root/simple-tdd-with-11-steps
# EOF