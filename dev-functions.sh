#!/bin/bash

alias sync-jobs="vagrant rsync; vagrant ssh -c 'sudo ln -sf /vagrant/jobs/* /var/lib/laminar/cfg/jobs; sudo chmod +x /vagrant/jobs/*'"

laminarc() {
  CMD="laminarc $@";
  vagrant ssh -c "$CMD"
}