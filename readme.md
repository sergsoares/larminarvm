## Máquina virtual para uso do CI - Laminarc 

HOST: Debian/strech64

Dependencias instaladas
- docker
- docker-compose

#### Requisitos

- [Git](https://git-scm.com/)
- [Virtualbox](https://www.virtualbox.org/)
- [Vagrant](https://www.vagrantup.com/)

#### Execução

Como os software instalados, dentro do contexto do projeto executar o comando.

$ vagrant up

Esse comando irá executar o Vagrantfile, que tem como objetivo instalar o debian e provisionar o Laminar e docker através do Ansible.
Após finalizado, será iniciada a máquina e exposta a porta :8080 do localhost, onde é possível ter acesso a dashboard de visualização do CI para verificar as execuções.

### Sobre o LaminarCi

[Laminar Docs](https://laminar.ohwg.net/docs.html) é em uma tradução autoral da documentação:

Laminar é um serviço compacto e modular de integração contínua para linux.Ele é self-hosted e amigável para desenvolver, evitando uma interface de configuração web em favor de simples versionamento do arquivos de configurações e scripts. 

#### Conjunto de funções bash criadas para auxiliar testes

##### alias sync-jobs

Sincroniza os jobs criados dentro da pasta jobs do repositório, para a pasta 
var/lib/laminar/cfg/jobs do laminar e habilitar os scripts para serem executados pelo laminarc.

##### laminarc

Comando para executar jobs diretamente do shell da máquina host sem necessitar acessar via SSH a VM.

